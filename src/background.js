;(function() {
    'use strict';

    var serversList = [];
    var allowRequest = true;
    var globalTimeout;

    if(localStorage['list']) {

        serversList = JSON.parse(localStorage['list'])
    }

    function reinit(list, response) {

        var allow = typeof list == 'array';
        if(allow) {
            serversList = list;
            localStorage['list'] = JSON.stringify(list);
        }

        response(allow);
    }

    function ajax(data) {


        if(typeof data !== 'object') {

            return;
        }

        if(typeof data['url'] !== 'string') {

            return;
        }

        if(typeof data['response'] !== 'function') {

            data['response'] = function() {};
        }

        if(typeof data['method'] !== 'string') {

            data['method'] = 'GET';
        }

        var timeout = 30;
        if(data['timeout']) {

            timeout = data['timeout'];
        }

        var xhr = new XMLHttpRequest();
        xhr.timeout = (timeout * 1000);
        xhr.open(data['method'], data['url'], true);
        xhr.onreadystatechange = function() {

            var request = xhr.responseText;
            if(!(typeof data['type'] === 'undefined' || data['type'] === 'raw')) {

                request = JSON.parse(request);
            }

            data.response.apply(xhr, [request, xhr.readyState == 4, xhr.status]);
        }

        if(typeof data['data'] === 'object') {

            var _data = data['data'];
            var formData = new FormData();

            for (var keyData in _data) {

                if (_data.hasOwnProperty(keyData)) {

                    formData.append(keyData, _data[keyData]);
                }
            }

            xhr.send(formData);
        } else {

            xhr.send();
        }
    }

    function getServerStatus(url, response) {

        var h1 = 'http://';
        var h2 = 'https://';
        if(!(url.substring(0, h1.length) == h1 || url.substring(0, h2.length) == h2)) {

            url = 'http://' + url;
        }

        ajax({
            'url': url,
            'timeout': 10,
            'response': function() {
                var xhr = this;
                response({
                    'url': url,
                    'status': xhr.status,
                    'serverStatus': (xhr.status == 200 ? 1 : 2),
                    'response': xhr.responseText
                });
            }
        });
    }

    function requestStatus(i) {

        getServerStatus(serversList[i]['url'], function(response) {

            if(allowRequest) {

                serversList[i]['status'] = response.serverStatus;
                localStorage['list'] = JSON.stringify(serversList);
            }
        });
    }

    function step() {

        for (var i in serversList) {

            requestStatus(i);
        }

        globalTimeout = setTimeout(function() {

            step();
        }, 5000);
    }

    step();

    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

        if(typeof request.type != 'undefined') {

            if(request.type == 'reinit') {

                clearTimeout(globalTimeout);
                allowRequest = false;

                reinit(request.list, function(_r) {

                    sendResponse(_r);
                    setTimeout(function() {

                        allowRequest = true;
                        step();
                    }, 10000);
                });
            }
        }
    });
})();

//chrome.browserAction.setIcon({'path': 'icons/icon16-error.png'});
//chrome.browserAction.setBadgeText({'text':""});
//chrome.browserAction.setBadgeBackgroundColor({'color': [0,0,0,255]});