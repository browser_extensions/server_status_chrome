;(function() {
    'use strict';

    document.addEventListener('DOMContentLoaded', function() {

        /*function getCurrentTabUrl(callback) {

            var queryInfo = {
                active: true,
                currentWindow: true
            };

            chrome.tabs.query(queryInfo, function(tabs) {

                callback(tabs[0].url);
            });
        }*/

        function sendData(data, response) {

            chrome.runtime.sendMessage(data, function sendMessage(_r) {

                response(_r);
            });
        }

        function refresh(list) {

            sendData({'type': 'reinit', 'list': list}, function() {});
        }
        
        (function () {

            if(localStorage['list']) {

                try {

                    var list = JSON.parse(localStorage['list']);
                    // 0 = не парсился/в процессе; 1 = ОК; 2 = ошибка
                    var serverStatus = 0;
                    var _s = ['', '-ok', '-error'];

                    var _tmp = document.querySelector('#list');

                    for(var i in list) {

                        if(i === 0) {

                            _tmp.innerHTML = '';
                        }

                        var url = list[i]['url'];
                        var h1 = 'http://';
                        var h2 = 'https://';
                        if(!(url.substring(0, h1.length) == h1 || url.substring(0, h2.length) == h2)) {

                            url = 'http://' + url;
                        }

                        _tmp.innerHTML = _tmp.innerHTML +
                            '<div class="server" id="server-' + i + '">' +
                            '<div class="left server-title"><a href="' + url + '" target="_blank">' + list[i]['url'] + '</a></div>' +
                            '<div class="left server-img"><img src="icons/icon-16' + (_s[list[i]['status']]) + '.png"> &nbsp; <i class="delete">X</i></div>' +
                            '<div class="clearfix"></div>' +
                            '</div>';
                    }

                    if(list.length > 0) {

                        _tmp = _tmp.querySelectorAll('.server .delete');
                        for(var i in list) {

                            _tmp[i].addEventListener('click', function() {

                                this.closest('.server').setAttribute('class', 'server hide');
                                delete list[i];
                                localStorage['list'] = JSON.stringify(list);
                            });
                        }
                    }
                } catch(e) {

                    localStorage['list'] = '';
                }
            } else {

                var list = {};
                document.querySelector('#list').innerHTML = 'Нет ни одного сервера';
            }

            function addServer(url) {

                var list = [];
                if(localStorage['list']) {

                    var list = JSON.parse(localStorage['list']);
                }

                list.push({
                    'url': url,
                    'status': 0
                });

                refresh(list);
            }
            function deleteServer(id) {

                alert(id);
                var list = [];
                if(localStorage['list']) {

                    var list = localStorage['list'];
                }

                list.splice(id, 1);

                refresh(list);
            }

            document.querySelector('#addServer button').addEventListener('click', function() {

                var input = this.closest('#addServer').querySelector('input');

                if(input.value) {

                    addServer(input.value);
                }
            });

        })();
    });
})();